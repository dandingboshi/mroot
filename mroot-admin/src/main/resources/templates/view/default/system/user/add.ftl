<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pageAlert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="user_form" action="${save}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

            <#include "/default/common/formAlert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.type")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="m-radio-inline">
                        <#list USER_TYPE_MAP?keys as key>
                            <label class="m-radio m-radio--state-success">
                                <input type="radio" name="type"
                   <#if !(user.type)??>checked</#if> <#if (user.type == key)>checked</#if>
                                       value="${key}">
                                ${USER_TYPE_MAP[key]}
                                <span></span>
                            </label>
                        </#list>
                    </div>
                    <span class="m-form__help">${I18N("message.form.type.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.username")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="username"
                           placeholder="${I18N("message.system.user.form.username.text")}"
                           value="${user.username}"
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.username.text.help")}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.password")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="password" class="form-control m-input" name="password"
                           placeholder="${I18N("message.system.user.form.password.text")}"
                           value=""
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.password.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.nickName")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="nickName"
                           placeholder="${I18N("message.system.user.form.nickName.text")}"
                           value="${user.nickName}"
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.nickName.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.email")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="email"
                           placeholder="${I18N("message.system.user.form.email.text")}"
                           value="${user.email}"
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.email.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.phone")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="phone"
                           placeholder="${I18N("message.system.user.form.phone.text")}"
                           value="${user.phone}"
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.phone.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.realName")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="realName"
                           placeholder="${I18N("message.system.user.form.realName.text")}"
                           value="${user.realName}"
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.realName.text.help")}</span>
                </div>
            </div>

        </div>

    <#-- 提交按钮 -->
               <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/user/userJs.ftl">
</@OVERRIDE>
<#include "/default/scriptPlugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
        FormValidation.formValidationUser();
    });
</script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
