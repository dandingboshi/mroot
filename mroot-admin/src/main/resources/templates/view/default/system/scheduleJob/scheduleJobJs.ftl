<script>

    /**
     * 定时任务提示信息
     */
    var ScheduleJobValidation = function () {

        var beanName_required = '${I18N("jquery.validation.system.scheduleJob.beanName.pattern")}';
        var cronExpression_required = '${I18N("jquery.validation.system.scheduleJob.cronExpression.pattern")}';
        var methodName_required = '${I18N("jquery.validation.system.scheduleJob.methodName.pattern")}';
        var name_pattern = '${I18N("jquery.validation.name.pattern")}';
        var params_required = '${I18N("jquery.validation.system.scheduleJob.params.pattern")}';
        var remark_pattern = '${I18N("jquery.validation.remark.pattern")}';
        var sort_range = '${I18N("jquery.validation.sort.range")}';
        var status_range = '${I18N("jquery.validation.status.range")}';
        var title_pattern = '${I18N("jquery.validation.title.pattern")}';

        return {


            getBeanNamePattern: function () {
                return beanName_required;
            },


            getCronExpressionPattern: function () {
                return cronExpression_required;
            },


            getMethodNamePattern: function () {
                return methodName_required;
            },


            getNamePattern: function () {
                return name_pattern;
            },


            getParamsPattern: function () {
                return params_required;
            },


            getRemarkPattern: function () {
                return remark_pattern;
            },


            getSortRange: function () {
                return sort_range;
            },


            getStatusRange: function () {
                return status_range;
            },


            getTitlePattern: function () {
                return title_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
