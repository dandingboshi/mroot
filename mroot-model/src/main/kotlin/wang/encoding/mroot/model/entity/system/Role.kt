/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Range
import java.io.Serializable

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern

/**
 * 角色实体类
 *
 * @author ErYang
 */
@TableName("system_role")
class Role : Model<Role>(), Serializable {

    companion object {

        private const val serialVersionUID = -5045484792000187199L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         * id编号
         */
        const val ID: String = "id"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param role  Role
         * @return Role
         */
        fun copy2New(role: Role): Role {
            val newRole = Role()
            newRole.id = role.id
            newRole.name = role.name
            newRole.title = role.title
            newRole.status = role.status
            newRole.gmtCreate = role.gmtCreate
            newRole.gmtCreateIp = role.gmtCreateIp
            newRole.gmtModified = role.gmtModified
            newRole.sort = role.sort
            newRole.remark = role.remark
            return newRole
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * id编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 角色标识
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.name.pattern")
    var name: String? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 角色名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var title: String? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    var sort: Long? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 角色备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9_，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var remark: String? = null

    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Role

        if (id != other.id) return false
        if (name != other.name) return false
        if (title != other.title) return false
        if (status != other.status) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (sort != other.sort) return false
        if (remark != other.remark) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (sort?.hashCode() ?: 0)
        result = 31 * result + (remark?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Role(id=$id, name=$name, title=$title, status=$status, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, sort=$sort, remark=$remark)"
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Role class

/* End of file Role.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/Role.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
