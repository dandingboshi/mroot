/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Range

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern
import java.io.Serializable


/**
 * 定时任务记录实体类
 *
 * @author ErYang
 */
@TableName("system_schedule_job_log")
class ScheduleJobLog : Model<ScheduleJobLog>(), Serializable {

    companion object {

        private const val serialVersionUID = 6578851439598479244L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         *表名
         */
        const val TABLE_NAME: String = "system_schedule_job_log"

        /**
         *表前缀
         */
        const val TABLE_PREFIX: String = "system_"

        /**
         * Spring Bean名称
         */
        const val BEAN_NAME: String = "bean_name"

        /**
         * Cron表达式
         */
        const val CRON_EXPRESSION: String = "cron_expression"

        /**
         * 失败信息
         */
        const val ERROR_MESSAGE: String = "error_message"

        /**
         * 创建时间
         */
        const val GMT_CREATE: String = "gmt_create"

        /**
         * 创建IP
         */
        const val GMT_CREATE_IP: String = "gmt_create_ip"

        /**
         * 修改时间
         */
        const val GMT_MODIFIED: String = "gmt_modified"

        /**
         * ID
         */
        const val ID: String = "id"

        /**
         * 方法名
         */
        const val METHOD_NAME: String = "method_name"

        /**
         * 参数
         */
        const val PARAMS: String = "params"

        /**
         * 备注
         */
        const val REMARK: String = "remark"

        /**
         * 定时任务ID
         */
        const val SCHEDULE_JOB_ID: String = "schedule_job_id"

        /**
         * 状态(1是成功，2是失败)
         */
        const val STATUS: String = "status"

        /**
         * 耗时(单位：毫秒)
         */
        const val TIMES: String = "times"

        /**
         * 名称
         */
        const val TITLE: String = "title"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param scheduleJobLog  ScheduleJobLog
         * @return ScheduleJobLog
         */
        fun copy2New(scheduleJobLog: ScheduleJobLog): ScheduleJobLog {
            val newScheduleJobLog = ScheduleJobLog()
            newScheduleJobLog.beanName = scheduleJobLog.beanName
            newScheduleJobLog.cronExpression = scheduleJobLog.cronExpression
            newScheduleJobLog.errorMessage = scheduleJobLog.errorMessage
            newScheduleJobLog.gmtCreate = scheduleJobLog.gmtCreate
            newScheduleJobLog.gmtCreateIp = scheduleJobLog.gmtCreateIp
            newScheduleJobLog.gmtModified = scheduleJobLog.gmtModified
            newScheduleJobLog.id = scheduleJobLog.id
            newScheduleJobLog.methodName = scheduleJobLog.methodName
            newScheduleJobLog.params = scheduleJobLog.params
            newScheduleJobLog.remark = scheduleJobLog.remark
            newScheduleJobLog.scheduleJobId = scheduleJobLog.scheduleJobId
            newScheduleJobLog.status = scheduleJobLog.status
            newScheduleJobLog.times = scheduleJobLog.times
            newScheduleJobLog.title = scheduleJobLog.title
            newScheduleJobLog.executionResult = scheduleJobLog.executionResult
            return newScheduleJobLog
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * Spring Bean名称
     */
    var beanName: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * Cron表达式
     */
    var cronExpression: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 失败信息
     */
    var errorMessage: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 方法名
     */
    var methodName: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 参数
     */
    var params: String? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 执行结果
     */
    var executionResult: String? = null

    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var remark: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 定时任务ID
     */
    var scheduleJobId: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 状态(1是成功，2是失败)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 耗时(单位：毫秒)
     */
    var times: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var title: String? = null


    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScheduleJobLog

        if (beanName != other.beanName) return false
        if (cronExpression != other.cronExpression) return false
        if (errorMessage != other.errorMessage) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (id != other.id) return false
        if (methodName != other.methodName) return false
        if (params != other.params) return false
        if (executionResult != other.executionResult) return false
        if (remark != other.remark) return false
        if (scheduleJobId != other.scheduleJobId) return false
        if (status != other.status) return false
        if (times != other.times) return false
        if (title != other.title) return false

        return true
    }

    override fun hashCode(): Int {
        var result = beanName?.hashCode() ?: 0
        result = 31 * result + (cronExpression?.hashCode() ?: 0)
        result = 31 * result + (errorMessage?.hashCode() ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (id?.hashCode() ?: 0)
        result = 31 * result + (methodName?.hashCode() ?: 0)
        result = 31 * result + (params?.hashCode() ?: 0)
        result = 31 * result + (executionResult?.hashCode() ?: 0)
        result = 31 * result + (remark?.hashCode() ?: 0)
        result = 31 * result + (scheduleJobId?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (times?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "ScheduleJobLog(beanName=$beanName, cronExpression=$cronExpression, errorMessage=$errorMessage, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, id=$id, methodName=$methodName, params=$params, executionResult=$executionResult, remark=$remark, scheduleJobId=$scheduleJobId, status=$status, times=$times, title=$title)"
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobLog class

/* End of file ScheduleJobLog.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/ScheduleJobLog.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
